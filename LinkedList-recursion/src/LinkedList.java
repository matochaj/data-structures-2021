public class LinkedList {
    private Link head;

    public LinkedList() {
//        head = null;
        head = new Link(1, new Link(2, new Link(3)));
    }

    public void appendIterative(int item) {
        Link ref = head;
        while (ref.next() != null) {
            ref = ref.next();
        }
        ref.setNext(new Link(item));
    }

    public void appendRecursive(int item) {
        head = appendRecursiveKernel(head, item);
    }

    private Link appendRecursiveKernel(Link ref, int item) {
        if (ref == null) {
            Link newLink = new Link(item);
            return newLink;
        } else {
            Link l = appendRecursiveKernel(ref.next(), item);
            ref.setNext(l);
            return ref;
        }
    }

    public String toString() {
        return toStringKernel(head);
    }

    private String toStringKernel(Link ref) {
        if (ref == null) {
            return "";
        } else {
            return ref.toString() + "\n" + toStringKernel(ref.next());
        }
    }

    public String toStringReverse() {
        return toStringReverseKernel(head);
    }

    private String toStringReverseKernel(Link ref) {
        if (ref == null) {
            return "";
        } else {
            return toStringReverseKernel(ref.next()) + "\n" + ref.toString();
        }
    }
}
