public class Link {
    private int e;
    private Link next;

    public Link(int e, Link next) {
        this.e = e;
        this.next = next;
    }
    public Link(int e) {
        this(e, null);
    }

    public Link next() {
        return next;
    }

    public void setNext(Link n) {
        next = n;
    }

    public String toString() {
        return "" + e;
    }
}
