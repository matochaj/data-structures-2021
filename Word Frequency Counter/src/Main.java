import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
	    String filename = "hamlet.txt";
        Scanner infile = new Scanner(new File(filename));
        Map<String, Integer> frequencies = new HashMap<>();
        int count=0;

        while (infile.hasNext()) {
//            String word = infile.next().toLowerCase();
            String word = cleanString(infile.next().toLowerCase());

            count++;

            if (frequencies.containsKey(word)) {
                // frequencies[word]++;
                frequencies.put(word, frequencies.get(word)+1);
            } else {
                // frequencies[word] = 1;
                frequencies.put(word, 1);
            }
        }

        int top1Percent = count/100;

        // Print out the top 1% most frequent words.
        for (String word : frequencies.keySet()) {
            if (frequencies.get(word) >= top1Percent) {
                System.out.println(word + " : " + frequencies.get(word));
            }
        }
    }

    public static String cleanString(String word) {
        String newWord = "";

        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) >= 'a' && word.charAt(i) <='z') {
                newWord += word.charAt(i);
            }
        }
        return newWord;
    }

}