// Max-heap implementation
// Modified slightly from OpenDSA
class Heap<E extends Comparable<E>> {
    private E[] heap; // Pointer to the heap array
    private int size;          // Maximum size of the heap
    private int n;             // Number of things now in heap

    // Constructor for empty Heap.
    public Heap(int size) {
        heap = (E[]) new Object[size];
        this.size = size;
        n = 0;
    }

    // Constructor supporting preloading of heap contents
    private Heap(E[] h, int num, int max) {
        heap = h;
        n = num;
        size = max;
        buildHeap();
    }

    // Insert val into heap
    public void insert(E key) {
        if (n >= size) {
            System.out.println("Heap is full");
            return;
        }
        int curr = n++;
        heap[curr] = key;  // Start at end of heap
        // Now sift up until curr's parent's key > curr's key
        while ((curr != 0) && (heap[curr].compareTo(heap[parent(curr)]) > 0)) {
            swap(curr, parent(curr));
            curr = parent(curr);
        }
    }

    // Remove and return maximum value
    public E removeMax() {
        if (n == 0) {
            return null;
        }  // Removing from empty heap
        swap(0, --n); // Swap maximum with last value
        siftDown(0);   // Put new heap root val in correct place
        return heap[n];
    }

    // Put element in its correct place
    private void siftDown(int pos) {
        if ((pos < 0) || (pos >= n)) {
            return;
        } // Illegal position
        while (!isLeaf(pos)) {
            int j = leftChild(pos);
            if ((j < (n - 1)) && (heap[j].compareTo(heap[j + 1]) < 0)) {
                j++; // j is now index of child with greater value
            }
            if (heap[pos].compareTo(heap[j]) >= 0) {
                return;
            }
            swap(pos, j);
            pos = j;  // Move down
        }
    }

    // Return current size of the heap
    public int heapSize() {
        return n;
    }

    // Return true if pos a leaf position, false otherwise
    private boolean isLeaf(int pos) {
        return (pos >= n / 2) && (pos < n);
    }

    // Return position for left child of pos
    private int leftChild(int pos) {
        if (pos >= n / 2) {
            return -1;
        }
        return 2 * pos + 1;
    }

    // Return position for right child of pos
    private int rightChild(int pos) {
        if (pos >= (n - 1) / 2) {
            return -1;
        }
        return 2 * pos + 2;
    }

    // Return position for parent
    private int parent(int pos) {
        if (pos <= 0) {
            return -1;
        }
        return (pos - 1) / 2;
    }

    // Heapify contents of Heap
    private void buildHeap() {
        for (int i = n / 2 - 1; i >= 0; i--) {
            siftDown(i);
        }
    }

    // Remove and return element at specified position
    public Comparable remove(int pos) {
        if ((pos < 0) || (pos >= n)) {
            return -1;
        } // Illegal heap position
        if (pos == (n - 1)) {
            n--;
        } // Last element, no work to be done
        else {
            swap(pos, --n); // Swap with last value
            update(pos);
        }
        return heap[n];
    }

    // Modify the value at the given position
    public void modify(int pos, E newVal) {
        if ((pos < 0) || (pos >= n)) {
            return;
        } // Illegal heap position
        heap[pos] = newVal;
        update(pos);
    }

    // The value at pos has been changed, restore the heap property
    private void update(int pos) {
        // If it is a big value, push it up
        while ((pos > 0) && (heap[pos].compareTo(heap[parent(pos)]) > 0)) {
            swap(pos, parent(pos));
            pos = parent(pos);
        }
        siftDown(pos); // If it is little, push down
    }

    private void swap(int index1, int index2) {
        E temp;
        temp = heap[index1];
        heap[index1] = heap[index2];
        heap[index2] = temp;
    }
}