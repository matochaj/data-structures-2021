import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Stack<Integer> operandStack = new AStack<>();
        Scanner kbd = new Scanner(System.in);

        String input = kbd.nextLine();

        while (!input.equals("=")) {
            try {
                int operand = Integer.parseInt(input);
                operandStack.push(operand);
            } catch (NumberFormatException e) {
                processOperator(input, operandStack);
            }

            input = kbd.nextLine();
        }

        System.out.println(operandStack.pop());
    }

    public static void processOperator(String operator, Stack<Integer> operandStack) {
        int op2 = operandStack.pop();
        int op1 = operandStack.pop();
        switch (operator) {
            case "+" -> operandStack.push(op1 + op2);
            case "-" -> operandStack.push(op1 - op2);
            case "*" -> operandStack.push(op1 * op2);
            case "/" -> operandStack.push(op1 / op2);
        }
    }
}