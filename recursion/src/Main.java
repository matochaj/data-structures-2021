public class Main {

    public static void main(String[] args) {
        int base = 2;
        int exp = 3;
        System.out.println(power(base, exp));

        System.out.println(maxDigit(735842));

        System.out.println(noX("abxcdexxfgxhxi"));

        System.out.println(countEvenDigits(123456789));
        System.out.println(countEvenDigits(12465520));

        System.out.println(count7(1738732));

        LList<String> myList = new LList<>();
        myList.append("A");
        myList.append("B");
        myList.append("C");
        myList.append("D");
        System.out.println(myList);
    }

    private static int count7(int n) {
        int count;

        if (n == 0) {
            count = 0;
        } else {
            int digit = n%10;
            int rest = n/10;

            if (digit == 7) {
                count = 1 + count7(rest);
            } else {
                count = 0 + count7(rest);
            }
        }

        return count;
    }

    private static int countEvenDigits(int n) {
        int count;

        if (n < 10) {
            if (n % 2 == 0) {
                count = 1;
            } else {
                count = 0;
            }
        } else {
            int rest = n / 10;
            int digit = n % 10;

            if (digit % 2 == 0) {
                count = 1 + countEvenDigits(rest);
            } else {
                count = 0 + countEvenDigits(rest);
            }
        }

        return count;
    }


    private static String noX(String str) {
        String result;

        if (str.isEmpty()) {
            result = "";
        } else {
            if (str.charAt(0) == 'x') {
                result = noX(str.substring(1));
            } else {
                result = str.charAt(0) + noX(str.substring(1));
            }
        }

        return result;
    }

    // n >= 0
    private static int maxDigit(int n) {
        int max;

        if (n < 10) {
            max = n;
        } else {
            int digit = n % 10;
            int rest = n / 10;

            int largestDigitAfter = maxDigit(rest);
            if (digit > largestDigitAfter) {
                max = digit;
            } else {
                max = largestDigitAfter;
            }
        }

        return max;
    }

    // Works for base > 0 and exp >= 0.
    private static int power(int base, int exp) {
        int result;

        if (exp == 0) {
            result = 1;
        } else {
            result = 2 * power(base, exp - 1);
        }

        return result;
    }


}
