import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class BinaryTree<E> {
    private BTNode<E> root;

    public BinaryTree() {
        BTNode<Integer> seventeen = new BTNode<>(17);
        BTNode<Integer> negativeFour = new BTNode<>(-4);
        BTNode<Integer> ninetyNine = new BTNode<>(99, seventeen, negativeFour);

        BTNode<Integer> oneTwentyThree = new BTNode<>(123);
        BTNode<Integer> twelve = new BTNode<>(12, null, oneTwentyThree);

        BTNode<Integer> fourtyTwo = new BTNode<>(42, ninetyNine, twelve);

        root = (BTNode<E>) fourtyTwo;
    }

    public BinaryTree(String filename) throws FileNotFoundException {
        root = readTreeKernel(new Scanner(new File(filename)));
    }

    private BTNode<E> readTreeKernel(Scanner inFile) {
        String data = inFile.nextLine();
        if (data.equals("null")) {
            return null;
        } else {
            BTNode left = readTreeKernel(inFile);
            BTNode right = readTreeKernel(inFile);

            return new BTNode(data, left, right);
        }
    }

    public int sum() {
        return sumKernel(root);
    }

    private int sumKernel(BTNode ref) {
        int sum;

        if (ref == null) {
            sum = 0;
        } else {
            sum = (int) ref.value() + sumKernel(ref.left()) + sumKernel(ref.right());
        }

        return sum;
    }

    public String toString() {
        return toStringKernel(root, 0);
    }

    // Reverse in-order traversal.
    private String toStringKernel(BTNode<E> ref, int depth) {
        String result;

        if (ref == null) {
            result = "";
            for (int i = 0; i < depth; i++) {
                result += "   ";
            }
            result += "null\n";
        } else {
            // Everything to my right.
            result = toStringKernel(ref.right(), depth + 1);

            // The stuff I am responsible for.
            for (int i = 0; i < depth; i++) {
                result += "   ";
            }
            result += ref.value() + "\n";

            // Everything to my left.
            result += toStringKernel(ref.left(), depth + 1);
        }

        return result;
    }


    public String toStringOld() {
        return toStringKernelOld(root);
    }

    // Pre-order traversal.
    private String toStringKernelOld(BTNode<E> ref) {
        String result;

        if (ref == null) {
            result = "null\n";
        } else {
            result = ref.value() + "\n" + toStringKernelOld(ref.left()) + toStringKernelOld(ref.right());
        }

        return result;
    }

    public int countNodes() {
        return countNodesKernel(root);
    }

    private int countNodesKernel(BTNode<E> ref) {
        int count;

        if (ref == null) {
            count = 0;
        } else {
            count = countNodesKernel(ref.left()) + countNodesKernel(ref.right()) + 1;
        }

        return count;
    }


    public int countLeaves() {
        return countLeavesKernel(root);
    }

    private int countLeavesKernel(BTNode<E> ref) {
        int count;

        if (ref == null) {
            count = 0;
        } else if (ref.left() == null && ref.right() == null) {
            count = 1;
        } else {
            count = countLeavesKernel(ref.left()) + countLeavesKernel(ref.right());
        }

        return count;
    }

    public void setDepth() {
        setDepth(root, 0);
    }

    private void setDepth(BTNode<E> ref, int depth) {
        // If ref is null, do nothing. (base case)
        // Otherwise...
        if (ref != null) {
            ((BTNode<Integer>) ref).setValue(depth);
            setDepth(ref.left(), depth + 1);
            setDepth(ref.right(), depth + 1);
        }
    }
}
