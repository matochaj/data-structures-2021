import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        BinaryTree<Integer> myTree = new BinaryTree<>();

        System.out.println(myTree);
        System.out.println(myTree.sum());

        System.out.println(myTree.countNodes());
        System.out.println(myTree.countLeaves());

        myTree.setDepth();
        System.out.println(myTree);

        BinaryTree<Integer> readTree = new BinaryTree("tree.txt");
        System.out.println(readTree);
    }
}
