// Binary tree node implementation: supports comparable objects
class BTNode<E> implements BinNode<E> {
    private E element;           // Element for this node`
    private BTNode<E> left;     // Pointer to left child
    private BTNode<E> right;    // Pointer to right child

    // Constructors
    BTNode() {
        left = right = null;
    }

    BTNode(E val) {
        left = right = null;
        element = val;
    }

    BTNode(E val, BTNode<E> l, BTNode<E> r) {
        left = l;
        right = r;
        element = val;
    }

    // Get and set the element value
    public E value() {
        return element;
    }

    public void setValue(E v) {
        element = v;
    }

    // Get and set the left child
    public BTNode<E> left() {
        return left;
    }

    public void setLeft(BTNode<E> p) {
        left = p;
    }

    // Get and set the right child
    public BTNode<E> right() {
        return right;
    }

    public void setRight(BTNode<E> p) {
        right = p;
    }

    // return TRUE if a leaf node, FALSE otherwise
    public boolean isLeaf() {
        return (left == null) && (right == null);
    }
}