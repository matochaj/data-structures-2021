// Binary Search Tree implementation
class BST<E extends Comparable<E>> {
    private BSTNode<E> root; // Root of the BST
    private int nodecount; // Number of nodes in the BST

    // constructor
    public BST() {
        root = null;
        nodecount = 0;
    }

    // Reinitialize tree
    public void clear() {
        root = null;
        nodecount = 0;
    }

    // Insert a record into the tree.
    // Records can be anything, but they must be Comparable
    // e: The record to insert.
    public void insert(E e) {
        root = inserthelp(root, e);
        nodecount++;
    }

    private BSTNode inserthelp(BSTNode rt, Comparable e) {
        if (rt == null) return new BSTNode(e);
        if (rt.value().compareTo(e) >= 0)
            rt.setLeft(inserthelp(rt.left(), e));
        else
            rt.setRight(inserthelp(rt.right(), e));
        return rt;
    }

    // Remove a record from the tree
    // key: The key value of record to remove
    // Returns the record removed, null if there is none.
    public E remove(E key) {
        E temp = findhelp(root, key); // First find it
        if (temp != null) {
            root = removehelp(root, key); // Now remove it
            nodecount--;
        }
        return temp;
    }

    private BSTNode removehelp(BSTNode rt, Comparable key) {
        if (rt == null) return null;
        if (rt.value().compareTo(key) > 0)
            rt.setLeft(removehelp(rt.left(), key));
        else if (rt.value().compareTo(key) < 0)
            rt.setRight(removehelp(rt.right(), key));
        else { // Found it
            if (rt.left() == null) return rt.right();
            else if (rt.right() == null) return rt.left();
            else { // Two children
                BSTNode temp = getmax(rt.left());
                rt.setValue(temp.value());
                rt.setLeft(deletemax(rt.left()));
            }
        }
        return rt;
    }

    private BSTNode getmax(BSTNode rt) {
        if (rt.right() == null) return rt;
        return getmax(rt.right());
    }

    private BSTNode deletemax(BSTNode rt) {
        if (rt.right() == null) return rt.left();
        rt.setRight(deletemax(rt.right()));
        return rt;
    }

    // Return the record with key value k, null if none exists
    // key: The key value to find
    public E find(E key) {
        return findhelp(root, key);
    }

    private E findhelp(BSTNode<E> rt, E key) {
        if (rt == null) return null;
        if (rt.value().compareTo(key) > 0)
            return findhelp(rt.left(), key);
        else if (rt.value().compareTo(key) == 0)
            return rt.value();
        else return findhelp(rt.right(), key);
    }

    // Return the number of records in the dictionary
    public int size() {
        return nodecount;
    }

    public String toString() {
        return toStringKernel(root, 0);
    }

    // Reverse in-order traversal.
    private String toStringKernel(BSTNode<E> ref, int depth) {
        String result;

        if (ref == null) {
            result = "";
            for (int i = 0; i < depth; i++) {
                result += "   ";
            }
            result += "null\n";
        } else {
            // Everything to my right.
            result = toStringKernel(ref.right(), depth + 1);

            // The stuff I am responsible for.
            for (int i = 0; i < depth; i++) {
                result += "   ";
            }
            result += ref.value() + "\n";

            // Everything to my left.
            result += toStringKernel(ref.left(), depth + 1);
        }

        return result;
    }
}